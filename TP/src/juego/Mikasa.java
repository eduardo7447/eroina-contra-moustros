package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Mikasa {

	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double angulo;
	private Image img1;
	private Image img2;
	private Image img3;
	private Image img4;
	
	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}

	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getAngulo() {
		return angulo;
	}
	Mikasa(double X, double Y){
		this.x = X;
		this.y = Y;
		this.alto = 50;
		this.ancho = 50;
		this.angulo =Math.PI/2;
		this.img1 = Herramientas.cargarImagen("MISAKA(abajo).png");
		this.img2 = Herramientas.cargarImagen("MISAKA(arriba).png");
		this.img3 = Herramientas.cargarImagen("MISAKA(derecha).png");
		this.img4 = Herramientas.cargarImagen("MISAKA(izquierda).png");
	}
	
	public void mostrarMikasa(Entorno e) {
//		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, this.angulo, Color.cyan);
		if (this.angulo==Math.PI/2)
			e.dibujarImagen(this.img1, this.x, this.y, 0, 1.5);
		if (this.angulo==1/Math.PI)
			e.dibujarImagen(this.img2, this.x, this.y, 0, 1.5);
		if (this.angulo==2*Math.PI)
			e.dibujarImagen(this.img3, this.x, this.y, 0, 1.5);
		if (this.angulo==Math.PI)
			e.dibujarImagen(this.img4, this.x, this.y, 0, 1.5);
		}
	
	public void caminar(Entorno e) {
		if (e.estaPresionada(e.TECLA_ABAJO)) {
			this.y++;
			this.angulo=Math.PI/2;
		}
		if (e.estaPresionada(e.TECLA_ARRIBA)) {
			this.y--;
			this.angulo=1/Math.PI;
		}
		if (e.estaPresionada(e.TECLA_DERECHA)) {
			this.x++;
			this.angulo=2*Math.PI;
		}
		if (e.estaPresionada(e.TECLA_IZQUIERDA)) {
			this.x--;
			this.angulo=Math.PI;
		}
	}
	
	
}
