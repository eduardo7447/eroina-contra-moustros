package juego.src.copy;

import java.awt.*;
import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;


import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Mikasa mikasa;
	private Kyojin[] kyojin; 
	private Image img;
	private Proyectil proyectil;
	public static boolean colision() {
		return false;
	}
	public Juego() {
		// Inicializa el objeto entorno
		
		this.entorno = new Entorno(this, "Prueba del Entorno", 800, 600);
		this.mikasa = new Mikasa (this.entorno.getWidth()/2, this.entorno.getHeight()/2);
		img = Herramientas.cargarImagen("FONDO.png");
		kyojin = new Kyojin[4];
		for (int i=0; i<kyojin.length; i++) {
			double rx = Math.random()*(700 - 100);
			double ry = Math.random()*(500 - 100);
			if (i>0) {
				if (kyojin[i-1].getX()+50!= rx && kyojin[i-1].getY()+50 != ry && (rx<750 || rx>50) && (ry<550 && ry>50))
					kyojin[i] = new Kyojin(rx,ry);
				else {
					rx = Math.random()*(750 - 50);
					ry = Math.random()*(550 - 50);
				}
			}else
				kyojin[i] = new Kyojin(rx,ry);
		}
		
		// Inicializar lo que haga falta para el juego
		// ...

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
		entorno.dibujarImagen(img, 0, 0, 0, 2);
		this.mikasa.mostrarMikasa(entorno);
		this.mikasa.caminar(entorno);
		for(int i=0; i<kyojin.length; i++) {
			if(kyojin[i]!= null) {
				kyojin[i].mostrarKyojin(entorno);
				kyojin[i].caminar();	
			}
			
			
		}
		for(int i = 0 ; i < kyojin.length ; i++) {
			if(kyojin[i]!= null && proyectil!= null) {
				if (kyojin[i].posicion().distance(proyectil.posicion())< 40 ) {
					kyojin[i] = null;
					proyectil= null ;
				}
			}
			
		}
		if (entorno.estaPresionada(entorno.TECLA_ESPACIO )) {
			proyectil = new Proyectil(mikasa.getX(),mikasa.getY(), mikasa.getAngulo());

		}
		if (proyectil!= null) {
			proyectil.mostrar(entorno);
		}
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
