package juego.src.copy;

import java.awt.Color;
import java.awt.Image;
import java.awt.Point;

import entorno.Entorno;

public class Proyectil {
	
	private double x;
	private double y;
	private double angulo;
	private Image img1;
	
	public Proyectil(double x , double y, double angulo) {
		this.x = x;
		this.y = y;
		this.angulo = angulo ;
	}
	
	public void mostrar(Entorno e) {

		e.dibujarRectangulo(x, y, 20, 10, angulo,Color.RED);
		mover();
	}
	public void mover() {
		if (this.angulo==Math.PI/2 ) {
			this.y=y+2;
		}
		if (this.angulo==1/Math.PI) {
			this.y = y-2;
		}
		if (this.angulo==2*Math.PI) {
			this.x=x+2;
		}
		if (this.angulo==Math.PI) {
			this.x=x-2;
		}
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getAngulo() {
		return angulo;
	}
	public Point posicion() {
		 return new Point((int)x, (int)y);
	}
}
