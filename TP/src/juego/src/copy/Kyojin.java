package juego.src.copy;
import java.awt.*;
import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

import java.awt.Color;
import java.awt.Image;
import java.awt.Point;

import entorno.Entorno;
import entorno.Herramientas;

public class Kyojin {

	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double angulo;
	private Image img1;
	private Image img2;
	private Image img3;
	private Image img4;
	
	
	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getAngulo() {
		return angulo;
	}
	
	Kyojin(double X, double Y){
		this.x = X;
		this.y = Y;
		this.alto = 50;
		this.ancho = 50;
		this.angulo =Math.PI/2;
		this.img1 = Herramientas.cargarImagen("KYOJIN(abajo).png");
		
	}
	
	public void mostrarKyojin(Entorno e) {
//		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, this.angulo, Color.cyan);
		if (this.angulo==Math.PI/2)
			e.dibujarImagen(this.img1, this.x, this.y, 0, 1.5);

		}
	
	public Point posicion() {
		 return new Point((int)x, (int)y);
	}
	
	public void caminar() {
		if (this.angulo==Math.PI/2) {
			this.y=y+0.5;
		}
		if (this.angulo==1/Math.PI) {
			this.x=x-.5;
		}
		if (this.angulo==2*Math.PI) {
			this.x=x+.5;
		}
		if (this.angulo==Math.PI) {
			this.x=x-.5;
		}
	}
}
