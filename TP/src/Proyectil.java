

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import juego.Mikasa;

public class Proyectil {
	
	private double x;
	private double y;
	private double angulo;
	private Image img1;
	
	public Proyectil(Mikasa mikasa ) {
		this.x = mikasa.getX();
		this.y = mikasa.getY() ;
		this.angulo = mikasa.getAngulo() ;
	}
	
	public void mostrar(Entorno e) {
		e.dibujarRectangulo(x, y, y, x, angulo, Color.RED);
	}
	
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getAngulo() {
		return angulo;
	}
}
